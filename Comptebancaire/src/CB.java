

/**
 * Classe d'instanciation d'un compte bancaire 
 * @author user
 * Un montant est attribué à la création du compte.

 *
 */

public class CB {
	/**
	 *montant: Le montant du compte bancaire.
	 *Il ne peut pas passer en dessous de 0.
	 */
	private int montant ;
	/**
	 * Constructeur de la classe CB
	 * À la création d'un CB, le montant est initialement fixé au "montant" passé en paramètre
	 * @param montant
	 * 		montant initial du compte
	 */

	public CB(int montant) {
		this.montant = montant;
	}
	/**
	 * Méthode retournant le montant du compte
	 * 
	 * @return montant
	 */
	public int getMontant() {
		return montant;
	}
	/**
	 * Crédite le solde du compte "montant" avec la somme m
	 * @param m
	 * 		Valeur à créditer sur le compte 
	 * @throws NombreNegatifException
	 * 		Si jamais on tente de créditer une valeur négative 
	 * 		
	 */
	public void credit(int m) throws NombreNegatifException{
		if(m<0) {	
			System.out.println("credit impossible");
			throw new NombreNegatifException();

		}
		else
			montant += m;

	}
	/**
	 * Débite le solde du compte "montant" de la somme m
	 * @param m
	 * 		Valeur à débiter du comte 
	 * @throws NombreNegatifException
	 * 			Si jamais on tente de débiter une valeur négative 
	 * @throws DecouvertException
	 * 			Pour éviter que le solde du compte soit négatif,
	 * 		    c'est-à-dire la valeur "m" est plus grande que "montant"
	 */

	public void debit(int m) throws NombreNegatifException,DecouvertException{
		if(m<0) {	
			System.out.println("debit impossible");
			throw new NombreNegatifException();

		}
		else {
			if(m>montant) throw new DecouvertException();
			else montant -= m;
		}
	}
}
